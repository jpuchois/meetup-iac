# Meetup-IAC

To use this demo you need : 
```
terraform >= 1.1.6
ansible >= 2.9.6
gcp sdk configured
```

```
ssh-keygen -t rsa -f ~/.ssh/ansible -C ansible -b 2048
```

copy the public key to gcp compute metadata



You need to also setup a terraform.tfvars file with this vars (update with your settings)
```
project_id       = "meetup-iac-sogeti"
gcp_region       = "europe-west1"
image            = "ubuntu-2004-focal-v20220204"
ssh_user         = "ansible"
private_key_path = "~/.ssh/ansible"
```
