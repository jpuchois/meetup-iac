locals {
  web_servers = {
    nginx-000-iac-meetup = {
      machine_type = "e2-micro"
      zone         = "europe-west1-b"
    }
  }
}


resource "google_service_account" "nginx" {
  account_id = "meetup-iac"
}



resource "google_compute_instance" "nginx" {
  for_each = local.web_servers

  name         = each.key
  machine_type = each.value.machine_type
  zone         = each.value.zone

  tags = ["front"]

  boot_disk {
    initialize_params {
      image = var.image
    }
  }

  network_interface {
    network    = module.vpc.network_id
    subnetwork = module.vpc.subnets_ids[0]
    access_config {
      // Ephemeral public IP
    }

  }

  service_account {
    email  = google_service_account.nginx.email
    scopes = ["cloud-platform"]
  }

  provisioner "remote-exec" {
    inline = ["echo 'Wait until SSH is ready'"]

    connection {
      type        = "ssh"
      user        = var.ssh_user
      private_key = file(var.private_key_path)
      host        = self.network_interface.0.access_config.0.nat_ip
    }
  }

  provisioner "local-exec" {
    command = "ansible-playbook  -i ${self.network_interface.0.access_config.0.nat_ip}, --private-key ${var.private_key_path} ansible/nginx.yml"
  }
}


output "nginx_ips" {
  value = {
    for k, v in google_compute_instance.nginx : k => "http://${v.network_interface.0.access_config.0.nat_ip}"
  }
}
