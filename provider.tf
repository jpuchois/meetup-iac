terraform {
  required_version = "~> 1.1.6"
  backend "gcs" {
  }
}

provider "google" {
  project = var.project_id
  region  = var.gcp_region
}

