resource "google_compute_firewall" "web" {
  name    = "web-access"
  network = module.vpc.network_name


  allow {
    protocol = "tcp"
    ports    = ["80"]
  }
  source_ranges           = ["0.0.0.0/0"]
  target_service_accounts = [google_service_account.nginx.email]
}


resource "google_compute_firewall" "ansible" {
  name    = "ansible-access"
  network = module.vpc.network_name


  allow {
    protocol = "tcp"
    ports    = ["22"]
  }
  source_ranges           = ["0.0.0.0/0"]
  target_service_accounts = [google_service_account.nginx.email]
}

