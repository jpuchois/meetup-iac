variable "project_id" {
  type = string
}

variable "gcp_region" {
  type = string
}

variable "image" {
  type = string
}

variable "ssh_user" {
  type = string
}

variable "private_key_path" {
  type = string
}

